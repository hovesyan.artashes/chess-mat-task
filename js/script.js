$(function(){
  var blackKingNearest = [];
  var whiteKingAttacks = [];
  var whiteQueenAttacks = [];
  var whiteElephantAttacks = [];
  var whiteHorseAttacks = [];
  const blackKingFigureSrc = 'img/Chess_kdt60.png';
  const whiteKingFigureSrc = 'img/Chess_klt60.png';
  const whiteQueenFigureSrc = 'img/Chess_qlt60.png';
  const whiteElephantFigureSrc = 'img/Chess_blt60.png';
  const whiteHorseFigureSrc = 'img/Chess_nlt60.png';
  figures=[blackKingFigureSrc,whiteKingFigureSrc,whiteQueenFigureSrc,whiteElephantFigureSrc,whiteHorseFigureSrc]
  figureNames=['Black King','White King','White Queen','White Elephant','White Horse'];
  createBoard();
  var figureNum = 0;
  appendMassage(figureNames[figureNum]);
  addFigure(figures[figureNum],figureNames[figureNum]);
  function createBoard(){
    var evenRow = false;
     for (var j = 0; j < 8; j++) {
       if(evenRow){
         n = 0;
         evenRow = false;
       }
       else {
         n = 1;
         evenRow = true;
       }
       var rowNum = j+1;
       var row = '<tr id="row-'+rowNum+'"></tr>'
       $('#board').append(row);
       for (var i = 0; i < 8; i++) {
         var field = $('<td>', {class: 'field'});
         field.addClass('free-field');
         $('#row-'+rowNum).append(field);
         field.attr('data-horizontal', j+1);
         field.attr('data-vertical', i+1);
         if(i%2 == n){
           field.addClass('brown-field')
         }
       }
      }
     }
      function addFigure(figureSrc, figureName) {
        $('.free-field').one( "click", function(event) {
          figureNum++;
          var currentVert = $(this).data('vertical');
          var currentHoriz = $(this).data('horizontal');
          if(figureNum==2){
            if(canWhiteKingAdd(currentHoriz,currentVert)==false){
              figureNum--;
              return false;
            };
          }
          $(this).removeClass('free-field');
          $(this).append('<img src ="'+figureSrc+'">');
          $('.field').off();
           if(figureNum<figures.length+1){
             if(figureNum==1){
               blackKingfunc(currentHoriz,currentVert);
             }
             if(figureNum==2){
               whiteKingfunc(currentHoriz,currentVert);
             }
             if(figureNum==3){
               whiteQueenfunc(currentHoriz,currentVert);
             }
             if(figureNum==4){
               whiteElephantfunc(currentHoriz,currentVert);
             }
             if(figureNum==5){
               whiteHorsefunc(currentHoriz,currentVert);
               checkMat();
               return false;
             }
            appendMassage(figureNames[figureNum])
            addFigure(figures[figureNum],figureNames);
           }
        })
      };

     function appendMassage(massage) {
       $('#boardTitle').html('Choose where the '+massage+' will be')
     }

     // Get black king possible steps

     function blackKingfunc(vert,horiz) {
       blackKingNearest.push({vert: vert, horiz: horiz})
       getKingNearest(vert,horiz,blackKingNearest);
     };

     // Get white king attaked attacked fields

     function whiteKingfunc(vert,horiz) {
       getKingNearest(vert,horiz,whiteKingAttacks);
     };

     // Get black king possible steps

     function getKingNearest(vert,horiz,arr) {
       if((vert+1)<=8){
         arr.push({vert: vert+1, horiz: horiz})
       }
       if((vert+1)<=8&&(horiz+1)<=8){
         arr.push({vert: vert+1, horiz: horiz+1})
       }
       if((vert+1)<=8&&(horiz-1)>0){
         arr.push({vert: vert+1, horiz: horiz-1})
       }
       if((vert-1)>0){
         arr.push({vert: vert-1, horiz: horiz})
       }
       if((vert-1)>0&&(horiz+1)<=8){
         arr.push({vert: vert-1, horiz: horiz+1})
       }
       if((vert-1)>0&&(horiz-1)>0){
         arr.push({vert: vert-1, horiz: horiz-1})
       }
       if((horiz+1)<=8){
         arr.push({vert: vert, horiz: horiz+1})
       }
       if((horiz-1)>0){
         arr.push({vert: vert, horiz: horiz-1})
       }
       return arr;
     };

     // Get white horse attaked attacked fields

     function whiteHorsefunc(vert,horiz) {
       if((vert+1)<=8&&(horiz+2)<=8){
         whiteHorseAttacks.push({vert: vert+1, horiz: horiz+2})
       }
       if((vert+1)<=8&&(horiz-2)>0){
         whiteHorseAttacks.push({vert: vert+1, horiz: horiz-2})
       }
       if((vert+2)<=8&&(horiz+1)<=8){
         whiteHorseAttacks.push({vert: vert+2, horiz: horiz+1})
       }
       if((vert+2)<=8&&(horiz-1)>0){
         whiteHorseAttacks.push({vert: vert+2, horiz: horiz-1})
       }
       if((vert-1)>0&&(horiz+2)<=8){
         whiteHorseAttacks.push({vert: vert-1, horiz: horiz+2})
       }
       if((vert-1)>0&&(horiz-2)>0){
         whiteHorseAttacks.push({vert: vert-1, horiz: horiz-2})
       }
       if((vert-2)>0&&(horiz+1)<=8){
         whiteHorseAttacks.push({vert: vert-2, horiz: horiz+1})
       }
       if((vert-2)>0&&(horiz-1)>0){
         whiteHorseAttacks.push({vert: vert-2, horiz: horiz-1})
       };
     };

     // Get white queen attaked attacked fields


     function whiteQueenfunc(vert,horiz){
       for (var i = 1; i < vert; i++) {
         whiteQueenAttacks.push({vert: vert-i, horiz: horiz,})
         if((horiz-i)>0){
         whiteQueenAttacks.push({vert: vert-i, horiz: horiz-i,})
        }
       }
       for (var i = 1; i < 9-vert; i++) {
         whiteQueenAttacks.push({vert: vert+i, horiz: horiz,})
         if((horiz+i)<=8){
         whiteQueenAttacks.push({vert: vert+i, horiz: horiz+i,})
       }
       }
       for (var i = 1; i < horiz; i++) {
         whiteQueenAttacks.push({vert: vert, horiz: horiz-i,})
         if((vert+i)<=8){
         whiteQueenAttacks.push({vert: vert+i, horiz: horiz-i,})
       }
       }
       for (var i = 1; i < 9-horiz; i++) {
         whiteQueenAttacks.push({vert: vert, horiz: horiz+i,})
         if((vert-i)>0){
         whiteQueenAttacks.push({vert: vert-i, horiz: horiz+i,})
       }
       }
     };

     // Get white Elephant attaked attacked fields

     function whiteElephantfunc(vert,horiz){
       for (var i = 1; i < vert; i++) {
         if((horiz-i)>0){
         whiteElephantAttacks.push({vert: vert-i, horiz: horiz-i,})
        }
       }
       for (var i = 1; i < 9-vert; i++) {
         if((horiz+i)<=8){
         whiteElephantAttacks.push({vert: vert+i, horiz: horiz+i,})
       }
       }
       for (var i = 1; i < horiz; i++) {
         if((vert+i)<=8){
         whiteElephantAttacks.push({vert: vert+i, horiz: horiz-i,})
       }
       }
       for (var i = 1; i < 9-horiz; i++) {
         if((vert-i)>0){
         whiteElephantAttacks.push({vert: vert-i, horiz: horiz+i,})
       }
       }
     }

      // Check if white king kan be in current field

     function canWhiteKingAdd(vert,horiz) {
       var cant = false;
       currentField = {
        vert: vert,
        horiz: horiz,
       }
       $.map(blackKingNearest, function(field) {
         if(field.vert == currentField.vert && field.horiz == currentField.horiz){
           alert("The king can't be here");
           cant = true;
         }
       });
       if(cant){
         return false;
       }
     };

     // Сheck if there is mate

     function checkMat(){
       var attackedFieldsNum = 0;
       var possibleFields = blackKingNearest;
       var attackedFields = [];
       attackedFields = whiteKingAttacks.concat(whiteQueenAttacks).concat(whiteElephantAttacks).concat(whiteHorseAttacks);
       for (var j = 0; j < blackKingNearest.length; j++) {
         for (var i = 0; i < attackedFields.length; i++) {
           if((blackKingNearest[j].vert == attackedFields[i].vert)&&(blackKingNearest[j].horiz == attackedFields[i].horiz)){
             attackedFieldsNum++;
             break;
           }
         }
       }
       setTimeout(function(){
         if(attackedFieldsNum==blackKingNearest.length){
           alert('Mat');
         }
         else {
           alert('Mat Chka');
         }
       }, 300);
     }
})
